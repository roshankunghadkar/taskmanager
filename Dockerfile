FROM ubuntu:18.04

ENV TZ=Asia/Kolkata \
    DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt install python3-pip git -y
RUN git clone https://gitlab.com/roshankunghadkar/taskmanager 
RUN pip3 install -r taskmanager/requirements.txt

CMD [ "python3", "./taskmanager/AdminTask.py" ]