*** Settings ***
Library  SeleniumLibrary
Resource  ../Resources/Task_Resources.robot
Library  DataDriver  ../TestData/TestCasesForRobo.xls   sheet_name=MembersLogin
Suite Setup       Open My Browser   ${URL}
Suite Teardown    Close Browsers
Test Template   Login user

*** Variables ***

*** Test Cases ***
Member Login

*** Keywords ***
Login user
        [Arguments]     ${username}     ${password}
        should match regexp  ${username}   ^.{4,12}$                   #username must between 4-12 characters
        should match regexp  ${password}   ^.{4,15}$                   #password must between 4-15 characters
        should match regexp  ${password}   ^[ A-Za-z0-9_@.!#&-]*$      #password contains only specific characters
        Open Login Page
        Enter Username      ${username}
        Enter Password      ${password}
        Click Login Button
        user Should be visible   ${username}
